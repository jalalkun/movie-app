package com.jalalkun.movieapp.models

data class Genre(
    val id: Int,
    val name: String
)