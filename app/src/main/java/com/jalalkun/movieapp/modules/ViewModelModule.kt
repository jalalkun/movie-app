package com.jalalkun.movieapp.modules

import com.jalalkun.movieapp.ui.MovieAppViewModel
import org.koin.dsl.module

val viewmodelModule = module {
    single { MovieAppViewModel(get()) }
}