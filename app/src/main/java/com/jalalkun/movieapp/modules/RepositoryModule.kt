package com.jalalkun.movieapp.modules

import com.jalalkun.movieapp.repository.ApiRepository
import com.jalalkun.movieapp.repository.ApiRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single<ApiRepository> { ApiRepositoryImpl(get()) }
}